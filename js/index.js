var oContent = new Vue({
  el: "#vue-content",
  name: 'transcript-editor',
  data: {
    transcript: oTranscript,
    currentTime: 0,
    matched: null,
    editKey: null,
    isTimeStamp: false,
  },
  watch: {
    matched: function() {
      if (this.matched) {
        document.getElementById("transcript-div").scroll(0, this.findPos(document.getElementById(this.matched)));
      }
    }
  },
  mounted() {
    setInterval(() => {
      if(!!player && !!player.getCurrentTime) {
        let ct = player.getCurrentTime();
        let minutes = (Math.floor(ct / 60)).toFixed();
        let seconds = (ct - minutes * 60).toFixed();
        this.currentTime = this.stringPaddingLeft(minutes, '0', 2) + ':' + this.stringPaddingLeft(seconds, '0', 2);
        this.matched = this.transcript[this.currentTime] ? this.currentTime : this.matched;
      }
    }, 1000)
  },
  methods: {
    onTranscriptClick(key) {
      player.pauseVideo();
      this.editKey = key;
    },
    stringPaddingLeft(string, pad, length) {
      return (new Array(length + 1).join(pad) + string).slice(-length);
    },
    findPos(obj) {
      var curtop = 0;
      if (obj.offsetParent) {
          do {
              curtop += obj.offsetTop;
          } while (obj = obj.offsetParent);
        return [curtop - 600]; // offset fix
      }
    }
  }
})

