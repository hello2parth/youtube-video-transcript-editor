# youtube-video-transcript-editor

## Features
* Focus on transcript shift as the video move
* Video stops playing if a user starts editing
* Easily add/edit/remove any word / punctuation

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

